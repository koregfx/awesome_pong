/*******************************************************************************************
*
*   raylib game: FINAL PONG - game template
*
*   developed by [ADRIAN]
*
*   This example has been created using raylib 1.0 (www.raylib.com)
*   raylib is licensed under an unmodified zlib/libpng license (View raylib.h for details)
*
*   Copyright (c) 2014 Ramon Santamaria (Ray San)
*
********************************************************************************************/

#include "raylib.h"

#define NUM_FRAMES  3



typedef enum GameScreen { LOGO, TITLE, MENU, GAMEPLAY1, GAMEPLAY2, ENDING } GameScreen;

int main()
{
    // Initialization
    //--------------------------------------------------------------------------------------
    int screenWidth = 800;
    int screenHeight = 450;
    char windowTitle[30] = "raylib game - FINAL PONG";
    
    GameScreen screen = LOGO;
    
    
    // TODO (0.5p): Definición de posibles variables a utilizar.................................................................(0.5p)
    
    
    
    // NOTE: Os damos ciertas variables ya declaradas. Algunas es aconsejable inicializarlas en este momento.
           

    Rectangle player;
    player.x = 15;
    player.y = screenHeight/2 - 25;
    player.width = 10;
    player.height = 50;
    
    int playerSpeedY = 10;
    
    Rectangle enemy;
    enemy.x = screenWidth - 15;
    enemy.y = screenHeight/2 - 25;
    enemy.width = 10;
    enemy.height = 50;
    
    float enemySpeedY = 10;
    
    Vector2 ballPosition;
    ballPosition.x = screenWidth/2;
    ballPosition.y = screenHeight/2;
    
    Vector2 ballSpeed;
    ballSpeed.x = 5;
    ballSpeed.y = 5;
    
    Vector2  powerUp1;
    powerUp1.x= -10;
    powerUp1.y= -10;
    
    Vector2  powerUp2;
    powerUp2.x= -10;
    powerUp2.y= -10;
    
    bool activePowerUp1 = false;
    bool activePowerUp2 = false;
    
    int probPowerUp1 = -1;
    int probPowerUp2 = -1;
    
    int timePowerUp1 = 0;
    int timePowerUp2 = 0;
    
    bool activeTime = false ;
    int countActivetime = 0;
    
    int lastTouch= 0;
    
    int ballRadius = 5;
    
    int playerLife = 10;
    int enemyLife = 10;
    
    bool pause = false;
    
    bool active = false;
    
    int partida = -1;
    
    int dibujo = -300;
    
    int paso = 0;
    
    int secondsCounter = 99;
    
    int framesCounterSec = 0;          // Para contar frames
    
    int framesCounter = 0;
    
    int frameCounterAlpha = 0;
    
    int frameSpeed = 1;
    
    int currentState = 0;
    
    int gameResult = -1;        // 0 - Loose, 1 - Win, -1 - Not defined
    
    
    
    
    
    int lettersCount = 0;
    
    float alpha = 1.0f;
    
    
    
    
    InitWindow(screenWidth, screenHeight, windowTitle);
    
    InitAudioDevice();
    
    Sound fxboh = LoadSound("resources/sound/boh.wav");
    Sound click = LoadSound("resources/sound/click1.wav");
    Sound punto = LoadSound("resources/sound/punto.wav");
    Sound finPartida = LoadSound("resources/sound/finPartida.wav");
    Sound music = LoadSound("resources/sound/musica.wav");
    Sound lifeUP = LoadSound("resources/sound/1up.wav");
    Sound intro = LoadSound("resources/sound/intro1.wav");
    Sound powerbarra = LoadSound("resources/sound/powerbarra.wav");
    bool activeMusic = true;
    
    // boton 1 player
    Texture2D button1 = LoadTexture("resources/images/button1Player.png");
    
    int frameHeight1 = button1.height/NUM_FRAMES;
    int currentState1 = 0;
    
    Rectangle sourceRec1 = { 0, 0, 200, frameHeight1 };
    Vector2 position1 = { screenWidth/2 - button1.width/2, 80 + screenHeight/3 - button1.height/2 };
    
    bool active1 = false;
    
    int framesCounter1 = 0;
    // boton 2 player
    Texture2D button2 = LoadTexture("resources/images/button2Player.png");
    
    int frameHeight2 = button2.height/NUM_FRAMES;
    int currentState2 = 0;
    
    Rectangle sourceRec2 = { 0, 0, 200, frameHeight2 };
    Vector2 position2 = { screenWidth/2 - button2.width/2, 2*screenHeight/3 - button2.height/2 };
    
    bool active2 = false;
    
    int framesCounter2 = 0;
    // boton EXIT
    Texture2D button3 = LoadTexture("resources/images/buttonExit.png");
    
    int frameHeight3 = button2.height/NUM_FRAMES;
    int currentState3 = 0;
    
    Rectangle sourceRec3 = { 0, 0, 200, frameHeight3 };
    Vector2 position3 = { screenWidth/2 - button3.width/2, -80 + 3*screenHeight/3 - button3.height/2 };
    
    bool active3 = false;
    
    int framesCounter3 = 0;
        // boton CONTINUE
    Texture2D button4 = LoadTexture("resources/images/buttonContinue.png");
    
    int frameHeight4 = button4.height/NUM_FRAMES;
    int currentState4 = 0;
    
    Rectangle sourceRec4 = { 0, 0, 200, frameHeight4 };
    Vector2 position4 = {  screenWidth/2 - button4.width/2, 80 + screenHeight/3 - button4.height/2 };
    
    bool active4 = false;
    
    int framesCounter4 = 0;
     // boton MENU
    Texture2D button5 = LoadTexture("resources/images/buttonMenu.png");
    
    int frameHeight5 = button5.height/NUM_FRAMES;
    int currentState5 = 0;
    
    Rectangle sourceRec5 = { 0, 0, 200, frameHeight5 };
    Vector2 position5 = { 2*screenWidth/4 - button5.width/2,  2*screenHeight/3 - button5.height/2 };
    
    bool active5 = false;
    
    int framesCounter5 = 0;
	// boton RESTART
    Texture2D button6 = LoadTexture("resources/images/buttonRestart.png");
    
    int frameHeight6 = button6.height/NUM_FRAMES;
    int currentState6 = 0;
    
    Rectangle sourceRec6 = { 0, 0, 200, frameHeight6 };
    Vector2 position6 = { 2*screenWidth/4 - button6.width/2, 80 + screenHeight/3 - button6.height/2 };
    
    bool active6 = false;
    
    int framesCounter6 = 0;
    bool pipo = false ;
    // NOTE: SI USAS TEXTURAS, SPRITES O SONIDOS, DECLARALOS A PARTIR DE ESTE PUNTO
    
    SetTargetFPS(60);
    //--------------------------------------------------------------------------------------
    
    // BUCLE PRINCIPAL DEL JUEGO
    while (!WindowShouldClose())    // CIERRA LA VENTANA SI DETECTA LA PULSACION DE LA TECLA ESC (escape)
    {
        SetSoundVolume(music, 0.6f);
        if(pipo == false)
        {
            PlaySound(intro);
            pipo = true;
            if(IsSoundPlaying(intro))
            {
                activeMusic = true;
            }
        }
            
        if(activeMusic == false && screen != LOGO)
        {
            
            PlaySound(music);
            if(IsSoundPlaying(music))
            {
                activeMusic = true;
            }
        }
        if(activeMusic == true)
        {
            if(pipo==true)
            {
                if(!IsSoundPlaying(music))
                {
                    activeMusic = false;
                }
            }
        }
        // Update
        //----------------------------------------------------------------------------------
        switch(screen) 
        {
            case LOGO: 
            {
                //Pantalla del Logo de la Empresa. Inventate uno, usa alguno de internet o el del RayLib.
                // TODO: Desarrolla la pantalla del LOGO..........................................................(0.5p)
                // EXTRA: Implementa algún tipo de efecto (sonido, fade in-out, animación, ...)
                
                
                
                if(framesCounter < 1.5f*60)
                {
                    framesCounter++;
                    
                }else
                {
                    framesCounter = 0;
                    screen = TITLE;
                }
                
                if(paso == 0)
                {
                
                    dibujo += 10;
                    
                    if(dibujo > 0)
                    {
                        paso = 1;
                    }
                }
                if (paso == 1)
                {
                    dibujo -= 3;
                    
                    if(dibujo < -60 )
                    {
                        paso = 2;
                    }
                }
                if(paso== 2)
                {
                    dibujo +=10;
                    
                    if(dibujo >= 0)
                    {
                        dibujo = 0;
                    }
                }
                
            } break;
            case TITLE: 
            {
                //Pantalla del Título del Juego.
                // TODO: Desarrolla la pantalla del Título del juego...............................................(0.5p)
                // EXTRA: Implementa algún tipo de efecto (sonido, fade in-out, animación, ...)
                
                // TODO: Muestra un mensaje (PRESS ENTER, por ejemplo) ............................................(0.2p)
                
                
                framesCounter++;

                if (framesCounter/12)       // Every 12 frames, one more letter!
                {
                    lettersCount++;
                    framesCounter = 0;
                }

                if (lettersCount >= 14)     // When all letters have appeared, just fade out everything
                {
                       alpha -= 0.02f;

                    if (alpha <= 0.0f)
                    {
                        alpha = 1.0f;
                        
                    } 
                
                }
                if( IsKeyPressed( KEY_ENTER))
                {
                        
                            screen = MENU;
                }
            } break;
            case MENU: 
            {
                if (active1) framesCounter1++;
                if (active2) framesCounter2++;
                if (active3) framesCounter3++;
                
                                pause = false;
                                playerLife = 10;
                                enemyLife = 10;
                                ballPosition.x = screenWidth/2;
                                ballPosition.y = screenHeight/2;
                                player.x = 25;
                                player.y = screenHeight/2 - 25;
                                enemy.x = 775;
                                enemy.y = screenHeight/2 - 25;
                                secondsCounter = 99;
                
                
                    if (CheckCollisionPointRec(GetMousePosition(), (Rectangle){ position1.x, position1.y, 200, frameHeight1}))
                    {
                        if (!active1) currentState1 = 1;
                        
                        if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON) && !active1)
                        {
                            currentState1 = 2;
                            active1 = true;
                            PlaySound(click);
                            screen = GAMEPLAY1;
                            
                        }
                        
                    }
                    else if (!active1) currentState1 = 0;
                        
                    if (framesCounter1 > 10)
                    {
                            active1 = false;
                            framesCounter1 = 0;
                            currentState1 = 0;
                    }
                    if (CheckCollisionPointRec(GetMousePosition(), (Rectangle){ position2.x, position2.y, 200, frameHeight2}))
                    {
                        if (!active2) currentState2 = 1;
                        
                        if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON) && !active2)
                        {
                            currentState2 = 2;
                            active2 = true;
                            PlaySound(click);
                            screen = GAMEPLAY2;
                        }
                        
                    }
                    else if (!active2) currentState2 = 0;
                        
                    if (framesCounter2 > 10)
                    {
                            active2 = false;
                            framesCounter2 = 0;
                            currentState2 = 0;
                    }
                    if (CheckCollisionPointRec(GetMousePosition(), (Rectangle){ position3.x, position3.y, 200, frameHeight3}))
                    {
                        if (!active3) currentState3 = 1;
                        
                        if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON) && !active3)
                        {
                            currentState3 = 2;
                            active3 = true;
                            PlaySound(click);
                            CloseWindow();  
                        }
                        }
                        else if (!active3) currentState3 = 0;
                        
                        if (framesCounter3 > 10)
                        {
                            active3 = false;
                            framesCounter3 = 0;
                            currentState3 = 0;
                        }
                
                
            } break;
            case GAMEPLAY1:
            { 
                SetSoundVolume(music, 0.1f);
                //Pantalla de Juego (GAMEPLAY)
                // TODO: Logica para el movimiento de la Bola......................................................(0.5p)
                
                // TODO: Logica para el movimiento del jugador.....................................................(0.5p)
                
                // TODO: Logica para el movimiento de la IA........................................................(0.5p)
                
                // TODO: Detección de colisiones Bola-Jugador......................................................(0.5p)
                
                // TODO: Detección de colisiones BOLA-IA...........................................................(0.5p)
                
                // TODO: Detección de colisiones BOLA-Limites......................................................(0.5p)
                
                // TODO: Lógica de barra de vidas (como si fuera la puntuación)......................................(1p)

                // TODO: Logica de tiempo de Juego.................................................................(0.5p)

                // TODO: Logica de final de Juego..................................................................(0.5p)
                
                // TODO: Incluye sonidos a tu juego (rebote de la bola y anotar puntos como mínimo) ...............(0.5p)
                
                // EXTRA: Incluye una pausa al Juego
                
                    
                    partida = 0;
                    
                    if(IsKeyPressed(KEY_P))
                    {
                        pause = !pause;
                    }
                    
                    if(pause == true )
                    {
                        
                        if (active4) framesCounter4++;
                        if (active5) framesCounter5++;
                        
                        if (CheckCollisionPointRec(GetMousePosition(), (Rectangle){ position4.x, position4.y, 200, frameHeight4}))
                        {
                            if (!active4) currentState4 = 1;
                            
                            if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON) && !active4)
                            {
                                currentState4 = 2;
                                active4 = true;
                                PlaySound(click);
                                pause = !pause;
                                
                            }
                            
                        }
                        else if (!active4) currentState4 = 0;
                            
                        if (framesCounter4 > 10)
                        {
                                active4 = false;
                                framesCounter4 = 0;
                                currentState4 = 0;
                        }
                          
                        
                        if (CheckCollisionPointRec(GetMousePosition(), (Rectangle){ position5.x, position5.y, 200, frameHeight5}))
                        {
                            if (!active5) currentState5 = 1;
                            
                            if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON) && !active5)
                            {
                                currentState5 = 2;
                                active5 = true;
                                PlaySound(click);
                                screen = MENU;
                                
                            }
                            
                        }
                        else if (!active5) currentState5 = 0;
                            
                        if (framesCounter5 > 10)
                        {
                                active5 = false;
                                framesCounter5 = 0;
                                currentState5 = 0;
                        }
                               
                         
                        
                        
                        if (CheckCollisionPointRec(GetMousePosition(), (Rectangle){ position3.x, position3.y, 200, frameHeight3}))
                        {
                        if (!active3) currentState3 = 1;
                        
                        if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON) && !active3)
                        {
                            currentState3 = 2;
                            active3 = true;
                            PlaySound(click);
                            CloseWindow();  
                        }
                        }
                        else if (!active3) currentState3 = 0;
                        
                        if (framesCounter3 > 10)
                        {
                            active3 = false;
                            framesCounter3 = 0;
                            currentState3 = 0;
                        }
                     
                    }else
                    {
                        
                        ballPosition.x += ballSpeed.x;
                        ballPosition.y += ballSpeed.y;
                        
                        
                        
                        //LOGICA TIEMPO
                        //----------------------------------------------------------------------------------
                        
                             framesCounterSec++;
            
                            if(framesCounterSec >= (60/frameSpeed))
                            {
                                framesCounterSec = 0;
                                secondsCounter -= 1;
                            }
                            
                             //LOGICA POWERUP VIDA
                        //----------------------------------------------------------------------------------   
                           if(playerLife< 10 || enemyLife<10)
                            {
                                if(activePowerUp1 == false)
                                {
                                    probPowerUp1 = rand() % 300;
                                    if(probPowerUp1 == 1)
                                    {
                                        activePowerUp1 = true;
                                        powerUp1.x = 100 + rand () % 600;
                                        powerUp1.y = 50 + rand() % 350;
                                    }
                                }else
                                {
                                    if(timePowerUp1 <600)
                                    {
                                        timePowerUp1++;
                                    }else
                                    {
                                        timePowerUp1 = 0;
                                        activePowerUp1 = false;
                                        powerUp1.x= -10;
                                        powerUp1.y= -10;
                                        
                                    }
                                    if(CheckCollisionCircles(powerUp1, 15, ballPosition, ballRadius))
                                    {
                                        
                                        PlaySound (lifeUP);
                                        powerUp1.x= -10;
                                        powerUp1.y= -10;
                                        activePowerUp1 = false;
                                        if(lastTouch == 1)
                                        {
                                            playerLife ++;
                                            if(playerLife > 10)playerLife =10;
                                        }
                                        if(lastTouch == 2)
                                        {
                                            enemyLife++;
                                            if(enemyLife > 10)enemyLife = 10;
                                        }
                                    }
                                }
                            }
                            
                               //LOGICA POWERUP BARRA
                        //----------------------------------------------------------------------------------
                            if(activePowerUp2 == false)
                            {
                                probPowerUp2 = rand() % 100;
                                if(probPowerUp2 == 1)
                                {
                                    activePowerUp2 = true;
                                    powerUp2.x = 100 + rand () % 600;
                                    powerUp2.y = 50 + rand() % 350;
                                }
                                
                            }else
                            {
                                if(timePowerUp2 <600)
                                {
                                    timePowerUp2++;
                                }else
                                {
                                    timePowerUp2 = 0;
                                    activePowerUp2 = false;
                                    powerUp2.x= -10;
                                    powerUp2.y= -10;
                                    
                                }
                                if(CheckCollisionCircles(powerUp2, 15, ballPosition, ballRadius))
                                {
                                    PlaySound(powerbarra);
                                    powerUp2.x= -10;
                                    powerUp2.y= -10;
                                    activePowerUp2 = false;
                                    activeTime = true;
                                    if(lastTouch == 1)
                                        {
                                            player.height *= 2;
                                        }
                                        if(lastTouch == 2)
                                        {
                                            enemy.height *= 2;
                                        }
                                }
                            }
                            if(activeTime ==true)
                            {       
                                        
                                        if(countActivetime <14*60)
                                        {
                                            countActivetime ++;
                                        }else
                                        {
                                            activeTime =false;
                                        }
                                        
                            }else
                            {
                                
                                countActivetime = 0;
                                player.height = 50;
                                enemy.height = 50;
                                        
                            }
                            
                        
                    
                            //LOGICA JUGADOR 1
                        //----------------------------------------------------------------------------------
                           

                            if(IsKeyDown(KEY_W) || IsKeyDown(KEY_UP))
                            {
                                player.y -= playerSpeedY;
                            }
                            if(IsKeyDown(KEY_S)|| IsKeyDown(KEY_DOWN))
                            {
                                player.y += playerSpeedY;
                            }
                            if(player.y < 0) 
                            {
                                player.y = 0;
                                
                            }
                            if(player.y > screenHeight - player.height) 
                            {
                                player.y = screenHeight - player.height;
                            }
                          
                            
                        // LOGICA "IA"
                        //---------------------------------------------------------------------------------- 
                            enemySpeedY = 4.01f;
                            
                            if(ballPosition.x >  80 + screenWidth / 2 )
                            {
                                if(ballPosition.y < enemy.y)
                                {
                                    enemy.y -= enemySpeedY;
                                }
                                else if (ballPosition.y > enemy.y)
                                {
                                    enemy.y += enemySpeedY;
                                }
                            }
        
                            if(enemy.y < 0 ) 
                            {
                                enemy.y = 0;
                                
                            }
                            if(enemy.y > screenHeight - enemy.height) 
                            {
                                enemy.y = screenHeight - enemy.height;
                            }
                            
                            
                        // LOGICA PELOTA
                        //----------------------------------------------------------------------------------
                            
                            
                            
                            
                            if(ballPosition.y < ballRadius || ballPosition.y > screenHeight-ballRadius) 
                            {
                                ballSpeed.y *= -1;
                                PlaySound (fxboh);
                            }
                            if(CheckCollisionCircleRec(ballPosition, ballRadius, player))
                            {
                                ballSpeed.x *= -1;
                                PlaySound (fxboh);
                                lastTouch = 1;
                            }
                            if(CheckCollisionCircleRec(ballPosition, ballRadius, enemy))
                            {
                                ballSpeed.x *= -1;
                                PlaySound (fxboh);
                                lastTouch = 2;
                            }
                            
                            
                         // PUNTUACION
                        //----------------------------------------------------------------------------------
                            if(ballPosition.x < ballRadius) 
                            {
                                playerLife -= 1;
                                ballPosition.x = screenWidth/2;
                                ballPosition.y = screenHeight/2;
                                player.x = 25;
                                player.y = screenHeight/2 - 25;
                                enemy.x = 775;
                                enemy.y = screenHeight/2 - 25;
                                PlaySound(punto);
                                lastTouch = 0;
                                player.height = 50;
                                enemy.height = 50;
                                
                            }
                            if( ballPosition.x > screenWidth-ballRadius) 
                            {
                                enemyLife -= 1;
                                ballPosition.x = screenWidth/2;
                                ballPosition.y = screenHeight/2;
                                player.x = 25;
                                player.y = screenHeight/2 - 25;
                                enemy.x = 775;
                                enemy.y = screenHeight/2 - 25;
                                PlaySound(punto);
                                lastTouch = 0;
                                player.height = 50;
                                enemy.height = 50;
                                
                            }
                            if(playerLife <= 0 || enemyLife <=0)
                            {
                                
                                screen = ENDING;
                                PlaySound(finPartida);
                            }
                            if(secondsCounter == 0)
                            {
                                screen = ENDING;
                                secondsCounter = 99;
                                PlaySound(finPartida);
                            }

                    }

            } break;
            case GAMEPLAY2:
            { 
                    SetSoundVolume(music, 0.1f);
                    partida = 1;
                    enemySpeedY = playerSpeedY;
                    
                            
                    if(IsKeyPressed(KEY_P))
                    {
                        pause = !pause;
                    }
                    
                    if(pause == true )
                    {
                        
                        if (active4) framesCounter4++;
                        if (active5) framesCounter5++;
                        if (active3) framesCounter3++;
                        
                        if (CheckCollisionPointRec(GetMousePosition(), (Rectangle){ position4.x, position4.y, 200, frameHeight4}))
                        {
                            if (!active4) currentState4 = 1;
                            
                            if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON) && !active4)
                            {
                                currentState4 = 2;
                                active4 = true;
                                PlaySound(click);
                                pause = !pause;
                                
                            }
                            
                        }
                        else if (!active4) currentState4 = 0;
                            
                        if (framesCounter4 > 10)
                        {
                                active4 = false;
                                framesCounter4 = 0;
                                currentState4 = 0;
                        }
                          
                          
                         if (CheckCollisionPointRec(GetMousePosition(), (Rectangle){ position5.x, position5.y, 200, frameHeight5}))
                        {
                            if (!active5) currentState5 = 1;
                            
                            if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON) && !active5)
                            {
                                currentState5 = 2;
                                active5 = true;
                                PlaySound(click);
                                screen = MENU;
                                
                            }
                            
                        }
                        else if (!active5) currentState5 = 0;
                            
                        if (framesCounter5 > 10)
                        {
                                active5 = false;
                                framesCounter5 = 0;
                                currentState5 = 0;
                        }
                        
                        
                        
                        if (CheckCollisionPointRec(GetMousePosition(), (Rectangle){ position3.x, position3.y, 200, frameHeight3}))
                        {
                        if (!active3) currentState3 = 1;
                        
                        if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON) && !active3)
                        {
                            currentState3 = 2;
                            active3 = true;
                            PlaySound(click);
                            CloseWindow();  
                        }
                        }
                        else if (!active3) currentState3 = 0;
                        
                        if (framesCounter3 > 10)
                        {
                            active3 = false;
                            framesCounter3 = 0;
                            currentState3 = 0;
                        }
                                
                    }else
                    {
                        
                        
                        ballPosition.x += ballSpeed.x;
                        ballPosition.y += ballSpeed.y;
                       
                        //LOGICA TIEMPO
                        //----------------------------------------------------------------------------------
                        
                         framesCounterSec++;
            
                            if(framesCounterSec >= (60/frameSpeed))
                            {
                                framesCounterSec = 0;
                                secondsCounter--;
                            }
                        
                        
                          //LOGICA POWERUP VIDA
                        //----------------------------------------------------------------------------------   
                            
                            if(playerLife< 10 || enemyLife<10)
                            {
                                if(activePowerUp1 == false)
                                {
                                    probPowerUp1 = rand() % 300;
                                    if(probPowerUp1 == 1)
                                    {
                                        activePowerUp1 = true;
                                        powerUp1.x = 100 + rand () % 600;
                                        powerUp1.y = 50 + rand() % 350;
                                    }
                                }else
                                {
                                    if(timePowerUp1 <600)
                                    {
                                        timePowerUp1++;
                                    }else
                                    {
                                        timePowerUp1 = 0;
                                        activePowerUp1 = false;
                                        powerUp1.x= -10;
                                        powerUp1.y= -10;
                                        
                                    }
                                    if(CheckCollisionCircles(powerUp1, 15, ballPosition, ballRadius))
                                    {
                                        
                                        PlaySound (lifeUP);
                                        powerUp1.x= -10;
                                        powerUp1.y= -10;
                                        activePowerUp1 = false;
                                        if(lastTouch == 1)
                                        {
                                            playerLife ++;
                                            if(playerLife > 10)playerLife =10;
                                        }
                                        if(lastTouch == 2)
                                        {
                                            enemyLife++;
                                            if(enemyLife > 10)enemyLife = 10;
                                        }
                                    }
                                }
                            }
                               //LOGICA POWERUP BARRA
                        //----------------------------------------------------------------------------------
                            if(activePowerUp2 == false)
                            {
                                probPowerUp2 = rand() % 100;
                                if(probPowerUp2 == 1)
                                {
                                    activePowerUp2 = true;
                                    powerUp2.x = 100 + rand () % 600;
                                    powerUp2.y = 50 + rand() % 350;
                                }
                                
                            }else
                            {
                                if(timePowerUp2 <600)
                                {
                                    timePowerUp2++;
                                }else
                                {
                                    timePowerUp2 = 0;
                                    activePowerUp2 = false;
                                    powerUp2.x= -10;
                                    powerUp2.y= -10;
                                    
                                }
                                if(CheckCollisionCircles(powerUp2, 15, ballPosition, ballRadius))
                                {
                                    PlaySound(powerbarra);
                                    powerUp2.x= -10;
                                    powerUp2.y= -10;
                                    activePowerUp2 = false;
                                    activeTime = true;
                                    if(lastTouch == 1)
                                        {
                                            player.height *= 2;
                                        }
                                        if(lastTouch == 2)
                                        {
                                            enemy.height *= 2;
                                        }
                                }
                            }
                            if(activeTime ==true)
                            {       
                                        
                                        if(countActivetime <14*60)
                                        {
                                            countActivetime ++;
                                        }else
                                        {
                                            activeTime =false;
                                        }
                                        
                            }else
                            {
                                
                                countActivetime = 0;
                                player.height = 50;
                                enemy.height = 50;
                                        
                            }
                            
                        //LOGICA JUGADOR 1
                        //----------------------------------------------------------------------------------
                           

                            if(IsKeyDown(KEY_W))
                            {
                                player.y -= playerSpeedY;
                            }
                            if(IsKeyDown(KEY_S))
                            {
                                player.y += playerSpeedY;
                            }
                            if(player.y < 0) 
                            {
                                player.y = 0;
                                
                            }
                            if(player.y > screenHeight - player.height) 
                            {
                                player.y = screenHeight - player.height;
                            }
                            
                        // LOGICA PLAYER 2
                        //---------------------------------------------------------------------------------- 

                            if( IsKeyDown(KEY_UP))
                            {
                                enemy.y -= enemySpeedY;
                            }
                            if( IsKeyDown(KEY_DOWN))
                            {
                                enemy.y += enemySpeedY;
                            }
                            if(enemy.y < 0 ) 
                            {
                                enemy.y = 0;
                                
                            }
                            if(enemy.y > screenHeight - enemy.height) 
                            {
                                enemy.y = screenHeight - enemy.height;
                            }
                            
                            
                        // LOGICA PELOTA
                        //----------------------------------------------------------------------------------

                            if(ballPosition.y < ballRadius || ballPosition.y > screenHeight-ballRadius) 
                            {
                                ballSpeed.y *= -1;
                                PlaySound (fxboh);
                                lastTouch = 1;
                            }
                            if(CheckCollisionCircleRec(ballPosition, ballRadius, player) || CheckCollisionCircleRec(ballPosition, ballRadius, enemy))
                            {
                                ballSpeed.x *= -1;
                                PlaySound (fxboh);
                                lastTouch = 2;
                            }
                            
                        // PUNTUACION
                        //----------------------------------------------------------------------------------
                            if(ballPosition.x < ballRadius) 
                            {
                                playerLife -= 1; 
                                ballPosition.x = screenWidth/2;
                                ballPosition.y = screenHeight/2;
                                player.x = 25;
                                player.y = screenHeight/2 - 25;
                                enemy.x = 775;
                                enemy.y = screenHeight/2 - 25;
                                PlaySound(punto);
                                player.height = 50;
                                enemy.height = 50;
                                
                            }
                            if( ballPosition.x > screenWidth-ballRadius) 
                            {
                                enemyLife -= 1;
                                ballPosition.x = screenWidth/2;
                                ballPosition.y = screenHeight/2;
                                player.x = 25;
                                player.y = screenHeight/2 - 25;
                                enemy.x = 775;
                                enemy.y = screenHeight/2 - 25;
                                PlaySound(punto);
                                player.height = 50;
                                enemy.height = 50;
                                
                            }
                            if(playerLife <= 0 || enemyLife <=0)
                            {
                                
                                screen = ENDING;
                                PlaySound(finPartida);
                                
                            }
                            if(secondsCounter == 0)
                            {
                                screen = ENDING;
                                secondsCounter = 99;
                                PlaySound(finPartida);
                            }
                            
                           
                       
                    }
            } break;
            
            case ENDING: 
            {
                //Pantalla de Final de Juego
                
                // TODO: Volver a Jugar / Salir del Juego..........................................................(0.5p)
                
                        player.height = 50;
                        enemy.height = 50;
                        
                        if (active6) framesCounter6++;
                        if (active5) framesCounter5++;
                        if (active3) framesCounter3++;
                        
                        if (CheckCollisionPointRec(GetMousePosition(), (Rectangle){ position6.x, position6.y, 200, frameHeight6}))
                        {
                            if (!active6) currentState6 = 1;
                            
                            if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON) && !active6)
                            {
                                currentState6 = 2;
                                active6 = true;
                                PlaySound(click);
                                if(partida == 0)screen = GAMEPLAY1;
                                if(partida == 1)screen = GAMEPLAY2;
                               
                                playerLife = 10;
                                enemyLife = 10;
                                ballPosition.x = screenWidth/2;
                                ballPosition.y = screenHeight/2;
                                player.x = 25;
                                player.y = screenHeight/2 - 25;
                                enemy.x = 775;
                                enemy.y = screenHeight/2 - 25;
                                
                            }
                            
                        }
                        else if (!active6) currentState6 = 0;
                            
                        if (framesCounter6 > 10)
                        {
                                active6 = false;
                                framesCounter6 = 0;
                                currentState6 = 0;
                        }
                          
                          
                         if (CheckCollisionPointRec(GetMousePosition(), (Rectangle){ position5.x, position5.y, 200, frameHeight5}))
                        {
                            if (!active5) currentState5 = 1;
                            
                            if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON) && !active5)
                            {
                                currentState5 = 2;
                                active5 = true;
                                PlaySound(click);
                                screen = MENU;
                                
                            }
                            
                        }
                        else if (!active5) currentState5 = 0;
                            
                        if (framesCounter5 > 10)
                        {
                                active5 = false;
                                framesCounter5 = 0;
                                currentState5 = 0;
                        }
                        
                        
                        
                        if (CheckCollisionPointRec(GetMousePosition(), (Rectangle){ position3.x, position3.y, 200, frameHeight3}))
                        {
                        if (!active3) currentState3 = 1;
                        
                        if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON) && !active3)
                        {
                            currentState3 = 2;
                            active3 = true;
                            PlaySound(click);
                            CloseWindow();  
                        }
                        }
                        else if (!active3) currentState3 = 0;
                        
                        if (framesCounter3 > 10)
                        {
                            active3 = false;
                            framesCounter3 = 0;
                            currentState3 = 0;
                        }
                
            } break;
            default: break;
        }
        //----------------------------------------------------------------------------------
        sourceRec1.y = currentState1*frameHeight1;
        sourceRec2.y = currentState2*frameHeight2;
        sourceRec3.y = currentState3*frameHeight3;
        sourceRec4.y = currentState4*frameHeight4;
        sourceRec5.y = currentState5*frameHeight5;
        sourceRec6.y = currentState6*frameHeight6;
        
                            
        // Draw
        //----------------------------------------------------------------------------------
        BeginDrawing();
        
            ClearBackground(RAYWHITE);
            
            switch(screen) 
            {
                case LOGO: 
                {
                    // Dibujar la pantalla de LOGO
                    
                    // TODO: Pantalla de LOGO.......................................................................(0.2p)
                    
                    DrawRectangle(screenWidth/2 - 128, screenHeight/2 - 128 + dibujo, 256, 256, BLACK);
                    DrawRectangle(screenWidth/2 - 112, screenHeight/2 - 112 + dibujo, 224, 224, RAYWHITE);
                    DrawText("raylib", screenWidth/2 - 44, screenHeight/2 + 48 + dibujo, 50, BLACK);
                    
                    
                } break;
                case TITLE: 
                {
                    // Dibujar la pantalla de titulo de juego
                    
                    // TODO: Pantalla de Titulo.....................................................................(0.2p)
                    
                    // TODO: Dibujar pulsar una tecla (PRESS ENTER).................................................(0.2p)
                    
                    DrawText(SubText("Pong By Adrian", 0, lettersCount), screenWidth/2 - MeasureText("Pong By Adrian", 50)/2 , screenHeight/3, 50, Fade(BLACK, alpha));
                    
                    DrawText("PRESS ENTER BOY", screenWidth/2 - MeasureText("PRESS ENTER BOY", 30)/2 , 325, 30, BLUE);
                
                } break;
                case MENU: 
                {
                    DrawText("PONG by Adrian", screenWidth/2 - MeasureText("PONG by Adrian", 50)/2 , 25, 50, BLACK);
                    
                    DrawTextureRec(button1, sourceRec1, position1, WHITE);
                    
                    DrawTextureRec(button2, sourceRec2, position2, WHITE);
                    
                    DrawTextureRec(button3, sourceRec3, position3, WHITE);
                    
                } break;
                case GAMEPLAY1:
                { 
                    case GAMEPLAY2:
                    {
                        
                    
                    // Dibujar la pantalla de Juego (Gameplay)
                    
                    // TODO: Dibuja a los jugadores.................................................................(0.2p)
                    
                    // TODO: Dibuja las barras de vida/energia......................................................(0.5p)
                    
                    // TODO: Dibuja la cuenta de Tiempo (cuenta atras)..............................................(0.5p)
                    
                        if(pause == true)
                        {
                            DrawText("PAUSE", screenWidth/2 - MeasureText("PAUSE", 50)/2 , 25, 50, BLACK);
                            
                            DrawTextureRec(button4, sourceRec4, position4, WHITE);
                            
                            DrawTextureRec(button5, sourceRec5, position5, WHITE);
                            
                            DrawTextureRec(button3, sourceRec3, position3, WHITE);
                            
                            
                        }else
                        {
                            DrawRectangleRec(player, GREEN);

                            DrawRectangleRec(enemy, RED);
                                    
                            DrawCircleV(ballPosition, ballRadius, BLUE);
                            
                            DrawCircleV(powerUp1, 15, BLACK);
                            DrawCircleV(powerUp2, 15, PURPLE);
                                  
                            
                            
                            DrawRectangle(70, 15, 20 * 10, 20, BLACK);
                            DrawRectangle(70, 15, 20 * playerLife, 20, GREEN);
                                    
                            
                            
                            DrawRectangle(screenWidth - 270, 15, 20 * 10, 20, BLACK);
                            DrawRectangle(screenWidth - 270, 15, 20 * enemyLife, 20, RED);
                            
                            DrawText(FormatText("%i", secondsCounter), screenWidth/2  - MeasureText(FormatText("%i", secondsCounter), 50)/2 , 25, 50, BLACK);
                           
                        }
                    }
                } break;
                case ENDING: 
                {
                    // Dibujar la pantalla final de juego
                    
                    // TODO: Dibujar las pantallas finales Ganar/Perder.............................................(0.5p)
                    DrawText("GAME OVER", screenWidth/2 - MeasureText("GAME OVER", 50)/2 , 25, 50, GREEN);
                    
                   
                        if(playerLife == enemyLife)
                        {
                            DrawText("TIE", screenWidth/2 - MeasureText("TIE", 25)/2 , 85, 25, BLUE);
                        }
                        if(playerLife < enemyLife)
                        {
                            DrawText("Red Player Win", screenWidth/2 - MeasureText("Red Player Win", 25)/2, 85, 25, RED);
                        }
                        if(playerLife > enemyLife)
                        {
                            DrawText("Green Player Win", screenWidth/2 - MeasureText("Green Player Win", 25)/2, 85, 25, GREEN);
                        }
                    
                    DrawTextureRec(button6, sourceRec6, position6, WHITE);
                    
                    DrawTextureRec(button5, sourceRec5, position5, WHITE);
                    
                    DrawTextureRec(button3, sourceRec3, position3, WHITE);
                    
                } break;
                default: break;
            }
        
            //DrawFPS(10, 10);
        
        EndDrawing();
        //----------------------------------------------------------------------------------
    }

    // De-Initialization
    //--------------------------------------------------------------------------------------
    
    
    // NOTE: Si usas alguna Textura o Sprite , descargalos aqui (Unload)
    UnloadSound (fxboh);
    UnloadSound (click);
    UnloadSound (finPartida);
    UnloadSound (punto);
    UnloadSound (music);
    UnloadSound (lifeUP);
    UnloadSound (intro);
    UnloadSound (powerbarra);
    
    UnloadTexture(button1);
    UnloadTexture(button2);
    UnloadTexture(button3);
    UnloadTexture(button4);
    UnloadTexture(button5);
    UnloadTexture(button6);
    
    CloseAudioDevice();
    
    CloseWindow();        // Close window and OpenGL context
    //--------------------------------------------------------------------------------------
    
    return 0;
}




